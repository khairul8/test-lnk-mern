import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { logout } from '../../actions/auth';

const Navbar = ({ auth: { isAuthenticated }, logout }) => {
    const authLinks = (
        <ul>
            <li>
                <a onClick={logout} href="#!">
                    <i className="fas fa-sign-out-alt" />{' '}
                    <span className="hide-sm">Logout</span>
                </a>
            </li>
        </ul>
    );

    const guestLinks = (
        <ul>
            <li>
                <Link style={{ textDecoration: 'none' }} to="/register">Register</Link>
            </li>
            <li>
                <Link style={{ textDecoration: 'none' }} to="/login">Login</Link>
            </li>
        </ul>
    );

    return (
        <nav className="navbar-ori bg-dark">
            <h3>
                <Link to="/" style={{ textDecoration: 'none' }}>
                    TEST-INK-MERN
                </Link>
            </h3>
            <Fragment>{isAuthenticated ? authLinks : guestLinks}</Fragment>
        </nav>
    );
};

Navbar.propTypes = {
    logout: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    auth: state.auth
});

export default connect(mapStateToProps, { logout })(Navbar);

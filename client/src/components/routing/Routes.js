import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Dashboard from '../dashboard/Dashboard';
import NotFound from '../layout/NotFound';
import PrivateRoute from '../routing/PrivateRoute';
import Alert from '../layout/Alert';

const Routes = props => {
    return (
        <section className="container">
            <Alert />
            <Switch>
                <PrivateRoute exact path="/dashboard" component={Dashboard} />
                <Route component={NotFound} />
            </Switch>
        </section>
    );
};

export default Routes;

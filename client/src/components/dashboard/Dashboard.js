import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import angkaTerbilang from '@develoka/angka-terbilang-js';

const Dashboard = ({ auth: { user } }) => {
  const calculator = {
    displayValue: '0',
    firstOperand: null,
    waitingForSecondOperand: false,
    operator: null,
  };

  const [calc, setCalc] = useState(calculator);
  const [result, setResult] = useState(0);

  useEffect(() => {
    setResult(calc.displayValue);
  }, [calc])

  function inputDigit(digit) {
    const { displayValue, waitingForSecondOperand } = calc;
    if (waitingForSecondOperand === true) {
      setCalc({ ...calc, displayValue: digit, waitingForSecondOperand: false })
    } else {
      console.log(displayValue === '0' ? digit : displayValue + digit, "digit");
      setCalc({ ...calc, displayValue: displayValue === '0' ? digit : displayValue + digit })
    }
  }

  function inputDecimal(dot) {
    // If the `displayValue` does not contain a decimal point
    if (!calc.displayValue.includes(dot)) {
      // Append the decimal point
      const decimalPoint = calc.displayValue += dot;
      setCalc({ ...calc, displayValue: decimalPoint })
    }
  }

  function handleOperator(nextOperator) {
    const { firstOperand, displayValue, operator, waitingForSecondOperand } = calc
    const inputValue = parseFloat(displayValue);

    if (operator && waitingForSecondOperand) {
      setCalc({ ...calc, operator: nextOperator })
      return;
    }

    if (firstOperand == null) {
      setCalc({ ...calc, firstOperand: inputValue, waitingForSecondOperand: true, operator: nextOperator })
    } else if (operator) {
      const currentValue = firstOperand || 0;
      const result = performCalculation[operator](currentValue, inputValue);

      setCalc({ ...calc, displayValue: String(result), firstOperand: result, waitingForSecondOperand: true, operator: nextOperator })
    }
  }

  const performCalculation = {
    '/': (firstOperand, secondOperand) => firstOperand / secondOperand,

    '*': (firstOperand, secondOperand) => firstOperand * secondOperand,

    '+': (firstOperand, secondOperand) => firstOperand + secondOperand,

    '-': (firstOperand, secondOperand) => firstOperand - secondOperand,

    '=': (firstOperand, secondOperand) => secondOperand
  };

  function resetCalculator() {
    setCalc(calculator)
  }



  const clickHandler = (event) => {
    const { target } = event;
    if (!target.matches('button')) {
      return;
    }

    if (target.classList.contains('operator')) {
      handleOperator(target.value);
      // updateDisplay();
      return;
    }

    if (target.classList.contains('decimal')) {
      inputDecimal(target.value);
      // updateDisplay();
      return;
    }

    if (target.classList.contains('all-clear')) {
      resetCalculator();
      // updateDisplay();
      return;
    }

    if (target.classList.contains('translate')) {
      const terbilang = angkaTerbilang(calc.displayValue);
      setCalc({ ...calc, displayValue: terbilang })
      return;
    }

    inputDigit(target.value);
    // updateDisplay();
  }

  return (
    <Fragment>
      <div className="d-flex justify-content-center">
        <div class="calculator card">

          <textarea class="calculator-screen z-depth-1" value={result} disabled />

          <div class="calculator-keys">

            <button type="button" class="operator btn btn-info" value="+" onClick={clickHandler}>+</button>
            <button type="button" class="operator btn btn-info" value="-" onClick={clickHandler}>-</button>
            <button type="button" class="operator btn btn-info" value="*" onClick={clickHandler}>&times;</button>
            <button type="button" class="operator btn btn-info" value="/" onClick={clickHandler}>&divide;</button>

            <button type="button" value="7" class="btn btn-light waves-effect" onClick={clickHandler} >7</button>
            <button type="button" value="8" class="btn btn-light waves-effect" onClick={clickHandler} >8</button>
            <button type="button" value="9" class="btn btn-light waves-effect" onClick={clickHandler} >9</button>
            <button type="button" value="show" class="btn btn-primary waves-effect translate" onClick={clickHandler} >S</button>


            <button type="button" value="4" class="btn btn-light waves-effect" onClick={clickHandler}>4</button>
            <button type="button" value="5" class="btn btn-light waves-effect" onClick={clickHandler} >5</button>
            <button type="button" value="6" class="btn btn-light waves-effect" onClick={clickHandler}>6</button>


            <button type="button" value="1" class="btn btn-light waves-effect" onClick={clickHandler}>1</button>
            <button type="button" value="2" class="btn btn-light waves-effect" onClick={clickHandler}>2</button>
            <button type="button" value="3" class="btn btn-light waves-effect" onClick={clickHandler}>3</button>


            <button type="button" value="0" class="btn btn-light waves-effect" onClick={clickHandler}>0</button>
            <button type="button" class="decimal function btn btn-secondary" value="." onClick={clickHandler}>.</button>
            <button type="button" class="all-clear function btn btn-danger btn-sm" value="all-clear" onClick={clickHandler}>AC</button>

            <button type="button" class="equal-sign operator btn btn-light waves-effect" value="=" onClick={clickHandler}>=</button>

          </div>
        </div>
      </div>
    </Fragment>
  );
};

Dashboard.propTypes = {
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(
  Dashboard
);

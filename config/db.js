const mongoose = require("mongoose");
const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '../.env') })

const db = process.env.MONGOURI;

const connectDB = async () => {
    try {
        mongoose.set("strictQuery", false);
        await mongoose.connect(db, {
            useNewUrlParser: true,
        });
        console.log("MongoDB Is Connected");
    } catch (error) {
        console.log("error Connection", error.message);
        //if Error Connect
        process.exit(1);
    }
};

module.exports = connectDB;

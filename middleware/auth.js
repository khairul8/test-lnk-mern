const jwt = require("jsonwebtoken");
const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '../.env') })

module.exports = function (req, res, next) {
  //Get Token From Header

  const token = req.header("x-auth-token");

  //check if no token
  if (!token) {
    return res.status(401).json({ msg: "No Token,Auth Denied" });
  }

  try {
    const decode = jwt.verify(token, process.env.JWT_SECRET);

    req.user = decode.user;
    next();
  } catch {
    res.status(401).json({ msg: "Token is not Valid" });
  }
};

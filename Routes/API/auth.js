const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '../../.env') })

//Model
const User = require("../../model/User");
const History = require("../../model/History");

//@route   GET api/auth/logout
//@desc    logout user
//@access  Private
router.get("/logout", auth, async (req, res) => {
    try {
        //find history
        const token = req.header("x-auth-token");
        const history = await History.findOne({ sessionId: token });
        if (history) {
            history.dateLogout = Date.now();
            await history.save();
        }
        return res.status(200).json({ msg: "Logout Success" });
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }

});

//@route    GET api/auth
//@desc     User Route
//@access   Private
router.get("/", auth, async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select("-password");
        res.json(user);
    } catch (err) {
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

//@route    POST api/auth
//@desc     Aunthenticate User & Get Token
//@access   Public
router.post(
    "/",
    [
        check("username", "Please is required").exists(),
        check("password", "Please is required").exists(),
    ],
    async (req, res) => {
        const err = validationResult(req);
        if (!err.isEmpty()) {
            return res
                .status(400)
                .json({ status: false, timestamp: Date.now(), error: err.array() });
        }

        const { username, password } = req.body;

        try {
            //see if user exist
            let user = await User.findOne({ username });

            if (!user) {
                res.status(400).json({ errors: [{ message: "Invalid Credentials" }] });
            }

            const isMatch = await bcrypt.compare(password, user.password);

            if (!isMatch) {
                return res
                    .status(400)
                    .json({ errors: [{ msg: "Invalid Credentials" }] });
            }

            //return jsonwebtoken

            const payload = {
                user: {
                    id: user.id,
                },
            };

            jwt.sign(
                payload,
                process.env.JWT_SECRET,
                { expiresIn: 360000 },
                (err, token) => {
                    if (err) throw err;
                    //add history login
                    const history = new History({
                        user: user.id,
                        sessionId: token,
                        username: user.username,
                        dateLogin: Date.now(),
                    });
                    history.save();
                    res.json({
                        status: true,
                        timestamp: Date.now(),
                        message: "User Logged In",
                        token
                    });
                }
            );
            // res.send("User Registered");
        } catch (error) {
            console.log(error.message);
            res.status(500).send("server Error");
        }
    }
);

//@route    POST api/auth/register
//@desc     Register USer
//@access   Public
router.post(
    "/register",
    [
        check("name", "Name Is Required").not().isEmpty(),
        check("username", "Username is Required").not().isEmpty(),
        check(
            "password",
            "Please Enter a password with 6 or more characters"
        ).isLength({ min: 6 }),
    ],
    async (req, res) => {
        const err = validationResult(req);
        if (!err.isEmpty()) {
            return res
                .status(400)
                .json({ status: false, timestamp: Date.now(), error: err.array() });
        }

        const { name, username, password } = req.body;

        try {
            //see if user exist
            let user = await User.findOne({ username });

            if (user) {
                res.status(400).json({ errors: [{ message: "User Already exists" }] });
            }

            //inisialisasi to Model Mongo
            user = new User({
                name,
                username,
                password,
            });
            //encrypt password
            const salt = await bcrypt.genSalt(10);

            user.password = await bcrypt.hash(password, salt);
            await user.save();

            //return jsonwebtoken

            const payload = {
                user: {
                    id: user.id,
                },
            };

            jwt.sign(
                payload,
                process.env.JWT_SECRET,
                { expiresIn: 360000 },
                (err, token) => {
                    if (err) throw err;
                    const history = new History({
                        user: user.id,
                        username: user.username,
                        sessionId: token,
                        dateLogin: Date.now(),
                    });
                    history.save();
                    res.json({
                        status: true,
                        timestamp: Date.now(),
                        message: "User Registered",
                        token
                    });
                }
            );
            // res.send("User Registered");
        } catch (error) {
            console.log(error.message);
            res.status(500).send("server Error");
        }
    }
);

module.exports = router;

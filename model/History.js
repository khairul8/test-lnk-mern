const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const HistorySchema = new mongoose.Schema({
    sessionId: {
        type: String,
        required: true,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "users",
    },
    username: {
        type: String,
        required: true,
    },
    dateLogin: {
        type: Date,
        default: Date.now,
    },
    dateLogout: {
        type: Date,
        default: null,
    },
});

module.exports = History = mongoose.model("history", HistorySchema);